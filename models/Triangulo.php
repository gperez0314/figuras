<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "figura".
 *
 * @property int $id
 * @property int $lado
 * @property int|null $base
 * @property int|null $altura
 */
class Triangulo extends \app\models\Figura
{
    const DISCR = 'triangulo';

    public function init()
    {
        parent::init();
        $this->discr = self::DISCR;
        $this->numLados = 3;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'figura';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lado'], 'required'],
            [['lado'], 'integer'],
            [['base'], 'required'],
            [['base'], 'integer'],
            [['altura'], 'required'],
            [['altura'], 'integer'],
        ];
    }

    public function getArea() {
       return ($this->base * $this->altura)/2;
    }

    public function getPerimetro(){
       return $this->lado + $this->lado + $this->lado;
    }

    public function printr(){
       return '(Lado) : (' . $this->lado . ')';
    }
}
