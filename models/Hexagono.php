<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "figura".
 *
 * @property int $id
 * @property int $lado
 * @property int|null $hipotenusa
 * @property float|null $radio
 */
class Hexagono extends \app\models\Figura
{
    const DISCR = 'hexagono';

    public function init()
    {
        parent::init();
        $this->discr = self::DISCR;
        $this->numLados = 6;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'figura';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lado'], 'required'],
            [['lado'], 'integer'],
            [['radio'], 'required'],
            [['radio'], 'integer'],
            [['hipotenusa'], 'required'],
            [['hipotenusa'], 'integer'],
            [['apotema'], 'required'],
            [['apotema'], 'integer'],
        ];
    }

    public function getArea() {
       return ($this->getPerimetro() * $this->radio)/2;
    }

    public function getPerimetro(){
       return $this->lado * 6;
    }

    public function printr(){
       return '(Lado) : (' . $this->lado . ')';
    }
}
